package com.huesca.huescainvaders.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.graphics.RectF
import androidx.lifecycle.AndroidViewModel
import com.huesca.huescainvaders.model.Bullet
import com.huesca.huescainvaders.model.GameState
import com.huesca.huescainvaders.model.Invader
import com.huesca.huescainvaders.model.SpaceShip
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow

class GameViewModel(application: Application) : AndroidViewModel(application) {

    val width = application.applicationContext.resources.displayMetrics.widthPixels
    val height = application.applicationContext.resources.displayMetrics.heightPixels
    @SuppressLint("StaticFieldLeak")
    val context: Context = application.applicationContext
    var gameState: GameState = GameState.PAUSED


    val gameLoop = flow<GameState> {
            while (gameState != GameState.GAME_OVER) {
                emit(gameState)
                delay(10)
            }
            emit(gameState)
        }

    var score = 0

    var waves = 1

    var lives = 3

    var spaceShip:SpaceShip = SpaceShip(context,width,height)
    val invaders = mutableListOf<Invader>()
    var numInvaders = 0

    var spaceShipBullet = Bullet(height, 1200f, 40f)


    val invadersBullets = mutableListOf<Bullet>()
    var nextBullet = 0
    val maxInvaderBullets = 10

    private fun prepareLevel() {
        Invader.numberOfInvaders = 0
        numInvaders = 0
        for (column in 0..7) {
            for (row in 0..3 ) {
                invaders.add(Invader(context,
                    row,
                    column,
                    width,
                    height))

                numInvaders++
            }
        }
        for (i in 0 until maxInvaderBullets) {
            invadersBullets.add(Bullet(height))
        }
    }

    fun pause(){
        gameState = GameState.PAUSED
    }

    fun resume(){
        gameState = GameState.RUNNING
        prepareLevel()
    }

    fun update() {
        spaceShip.update()

        var lost = false

        var bumped = false

        for (invader in invaders) {

            if (invader.isVisible) {
                invader.update()

                if (invader.shootSpaceShip(spaceShip.position.left, spaceShip.width, waves)) {

                    if (invadersBullets[nextBullet].shoot(invader.position.left + invader.width / 2, invader.position.top, spaceShipBullet.down)) {
                        nextBullet++

                        if (nextBullet == maxInvaderBullets) {
                            nextBullet = 0
                        }
                    }
                }

                if (invader.position.left > width - invader.width || invader.position.left < 0) {
                    bumped = true
                }
            }
        }

        if (spaceShipBullet.isActive) {
            spaceShipBullet.update()
        }

        for (bullet in invadersBullets) {
            if (bullet.isActive) {
                bullet.update()
            }
        }

        if (bumped) {
            for (invader in invaders) {
                invader.dropDownAndChangeDirection(waves)
                if (invader.position.bottom >= height && invader.isVisible) {
                    lost = true;
                }
            }
        }

        if (spaceShipBullet.position.bottom < 0) {
            spaceShipBullet.isActive =false
        }

        for (bullet in invadersBullets) {
            if (bullet.position.top > height) {
                bullet.isActive = false
            }
        }

        if (spaceShipBullet.isActive) {
            for (invader in invaders) {
                if (invader.isVisible) {
                    if (RectF.intersects(spaceShipBullet.position, invader.position)) {

                        invader.isVisible = false
                        spaceShipBullet.isActive = false
                        Invader.numberOfInvaders --
                        score += 10

                        if (Invader.numberOfInvaders == 0) {
                            gameState = GameState.PAUSED
                            lives ++
                            invaders.clear()
                            invadersBullets.clear()
                            prepareLevel()
                            waves ++
                            break
                        }
                        break
                    }
                }
            }
        }

        for (bullet in invadersBullets) {
            if (bullet.isActive) {
                if (RectF.intersects(spaceShip.position, bullet.position)) {
                    bullet.isActive = false
                    lives --
                    if (lives == 0) {
                        lost = true
                        break
                    }
                }
            }
        }

        if (lost) {
            gameState = GameState.GAME_OVER
        }
    }

    fun reset() {
        lives = 3
        score = 0
        waves = 1
        invaders.clear()
        invadersBullets.clear()
    }

}
