package com.huesca.huescainvaders.view

import android.content.*
import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.huesca.huescainvaders.R
import com.huesca.huescainvaders.model.*
import com.huesca.huescainvaders.viewmodel.GameViewModel
import kotlinx.coroutines.launch

class GameView(context: Context): SurfaceView(context){

    init {
        setBackgroundColor(R.color.black)
    }

    private lateinit var viewModel: GameViewModel

    constructor(context: Context, gameViewModel: GameViewModel) : this(context){
        viewModel = gameViewModel
        (context as LifecycleOwner)
            .lifecycleScope
            .launch {
                viewModel.gameLoop.collect {
                    when (it){
                        GameState.RUNNING -> {
                            viewModel.update()
                            invalidate()
                        }
                        GameState.GAME_OVER -> {
                            findNavController().navigate(R.id.action_gameFragment_to_endFragment)
                        }
                        GameState.PAUSED -> {
                            invalidate()
                        }
                    }
                }
            }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val paint: Paint = Paint()

        paint.color = Color.argb(255, 0, 255, 0)

        canvas.drawBitmap(viewModel.spaceShip.bitmap, viewModel.spaceShip.position.left, viewModel.spaceShip.position.top, paint)

        for (invader in viewModel.invaders) {
            if (invader.isVisible) {
                    canvas.drawBitmap(Invader.bitmap, invader.position.left, invader.position.top, paint)
            }
        }

        if (viewModel.spaceShipBullet.isActive) {
            canvas.drawRect(viewModel.spaceShipBullet.position, paint)
        }

        for (bullet in viewModel.invadersBullets) {
            if (bullet.isActive) {
                canvas.drawRect(bullet.position, paint)
            }
        }

        paint.color = Color.argb(255, 255, 255, 255)
        paint.typeface = context.resources.getFont(R.font.space_invaders)
        paint.textSize = 50f
        canvas.drawText("Score: ${viewModel.score}", 50f, 75f, paint)
        canvas.drawText("Lives: ${viewModel.lives}   Wave: " + "${viewModel.waves}",
            (width - 550).toFloat(), 75f, paint)
    }

    override fun onTouchEvent(motionEvent: MotionEvent): Boolean {
        val motionArea = height - (height / 8)
        when (motionEvent.action and MotionEvent.ACTION_MASK) {

            MotionEvent.ACTION_POINTER_DOWN,
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE-> {
                viewModel.gameState = GameState.RUNNING

                if (motionEvent.y > motionArea) {
                    if (motionEvent.x > width / 2) {
                        viewModel.spaceShip.moving = SpaceShip.right
                    } else {
                        viewModel.spaceShip.moving = SpaceShip.left
                    }

                }

                if (motionEvent.y < motionArea) {
                    viewModel.spaceShipBullet.shoot(viewModel.spaceShip.position.left + viewModel.spaceShip.width / 2f,
                        viewModel.spaceShip.position.top, viewModel.spaceShipBullet.up)
                }
            }

            MotionEvent.ACTION_POINTER_UP, MotionEvent.ACTION_UP -> {
                if (motionEvent.y > motionArea) {
                    viewModel.spaceShip.moving = SpaceShip.stopped
                }
            }

        }
        return true
    }
}

