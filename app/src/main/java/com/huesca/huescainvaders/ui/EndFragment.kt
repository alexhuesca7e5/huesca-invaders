package com.huesca.huescainvaders.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.huesca.huescainvaders.R
import com.huesca.huescainvaders.databinding.FragmentEndBinding
import com.huesca.huescainvaders.viewmodel.GameViewModel

class EndFragment : Fragment() {

    private var _binding: FragmentEndBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentEndBinding.inflate(inflater, container, false)
        _binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button = binding.button2
        button.setOnClickListener {
            viewModel.reset()
            findNavController().navigate(R.id.action_endFragment_to_startFragment)
        }
        val score = binding.score
        score.text = viewModel.score.toString()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
