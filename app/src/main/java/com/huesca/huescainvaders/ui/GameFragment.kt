package com.huesca.huescainvaders.ui

import android.annotation.SuppressLint
import android.graphics.Point
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.huesca.huescainvaders.R
import com.huesca.huescainvaders.view.GameView
import com.huesca.huescainvaders.viewmodel.GameViewModel


class GameFragment : Fragment() {
    private val viewModel: GameViewModel by activityViewModels()

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val gameView = context?.let { GameView(it, viewModel) }
        return gameView
    }

    override fun onResume() {
        super.onResume()
        viewModel.resume()
    }

    override fun onPause() {
        super.onPause()
        viewModel.pause()
    }



}
