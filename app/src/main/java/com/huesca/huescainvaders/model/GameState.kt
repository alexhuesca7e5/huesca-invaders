package com.huesca.huescainvaders.model

enum class GameState {
    RUNNING,
    PAUSED,
    GAME_OVER
}