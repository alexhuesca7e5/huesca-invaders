package com.huesca.huescainvaders.model

import android.content.*
import android.graphics.*
import com.huesca.huescainvaders.*

class SpaceShip(context: Context, private val screenX: Int, screenY: Int) {

    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.space_ship)

    val width = screenX / 15f
    private val height = screenY / 15f

    val position = RectF(screenX / 2f, screenY-height, screenX/2 + width, screenY.toFloat())

    private val speed  = 450f

    companion object {
        const val stopped = 0
        const val left = 1
        const val right = 2
    }

    var moving = stopped

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt() , height.toInt() , false)
    }

    fun update() {
        val fps = 60

        if (moving == left && position.left > 0) {
            position.left -= speed / fps
        }
        else if (moving == right && position.left < screenX - width) {
            position.left += speed / fps
        }
        position.right = position.left + width
    }

}
