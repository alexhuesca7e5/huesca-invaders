package com.huesca.huescainvaders.model

import android.graphics.RectF

class Bullet(screenY: Int, private val speed: Float = 350f, bulletSize: Float = 20f) {

    val position = RectF()

    val up = 0
    val down = 1

    private var heading = -1

    private val width = 2
    private var height = screenY / bulletSize

    var isActive = false

    fun shoot(startX: Float, startY: Float, direction: Int): Boolean {
        if (!isActive) {
            position.left = startX
            position.top = startY
            position.right = position.left + width
            position.bottom = position.top + height
            heading = direction
            isActive = true
            return true
        }

        return false
    }

    fun update() {
        val fps = 60

        if (heading == up) {
            position.top -= speed / fps
        } else {
            position.top += speed / fps
        }

        position.bottom = position.top + height
    }
}